<?php

namespace A4Sex;

use PhpParser\Node\Stmt\If_;
use function Composer\Autoload\includeFile;

class ServicePathParser
{
    private array $items = [];

    public function parse(string $path): array
    {
        $this->items = explode('/', $path, 3);

        return $this->items;
    }

    public function project(string $path, ?string $default = null): ?string
    {
        $this->parse($path);
        if (count($this->items) == 3) {
            return $this->items[0];
        }

        return $default;
    }

    public function service(string $path, ?string $default = null): ?string
    {
        $this->parse($path);
        if (count($this->items) == 3) {
            return $this->items[1];
        }
        if (count($this->items) == 2) {
            return $this->items[0];
        }

        return $default;
    }

    public function action(string $path, ?string $default = null): string
    {
        $this->parse($path);
        if (count($this->items) == 3) {
            return $this->items[2];
        }
        if (count($this->items) == 2) {
            return $this->items[1];
        }
        if (count($this->items)) {
            return $this->items[0];
        }
        if ($default) {
            return $default;
        }

        return $path;
    }

}
