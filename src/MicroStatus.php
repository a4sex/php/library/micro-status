<?php

namespace A4Sex;

use A4Sex\KeyValueStorage;
use Exception;

class MicroStatus
{
    private ?string $session_project;
    private ?string $session_service;
    private ?string $session_action;

    private ?string $action;

    public function __construct(
        private readonly ServicePathParser $parser,
        private readonly KeyValueStorage $storage,
        private ?string $project = null,
        private ?string $service = null,
        private ?int $statusTtl = null,
        private ?string $prefix = null,
    ) {
        $this->setProject($project);
        $this->setService($service);
        $this->setAction();
    }

    /**
     * @param $prefix
     * @return $this
     */
    public function setPrefix($prefix): static
    {
        $this->prefix = $prefix;

        return $this;
    }

    /**
     * @param int|null $statusTtl
     * @return $this
     */
    public function setStatusTtl(?int $statusTtl): static
    {
        if ($statusTtl) {
            $this->statusTtl = $statusTtl;
        }

        return $this;
    }

    /**
     * Обертка для ключа
     * @param $key
     * @return string
     */
    public function wrapKey($key): string
    {
        if (!$this->prefix) {
            return $key;
        }
        return $this->prefix."__$key";
    }

    /**
     * Установить ID проекта
     * @param ?string $project ID проекта
     * @return $this
     */
    public function setProject(?string $project = 'default'): static
    {
        $this->project = $project;
        $this->session_project = $project;

        return $this;
    }

    /**
     * Установить ID сервиса
     * @param ?string $service ID сервиса
     * @return $this
     */
    public function setService(?string $service = 'service'): static
    {
        $this->service = $service;
        $this->session_service = $service;

        return $this;
    }

    /**
     * Установить ID действия или события
     * @param ?string $action ID действия или события
     * @return $this
     */
    public function setAction(?string $action = 'action'): static
    {
        $this->action = $action;
        $this->session_action = $action;

        return $this;
    }

    /**
     * @param ?string $path путь действия
     * @return $this
     */
    public function action(?string $path = 'action'): static
    {
        $this->session_project = $this->parser->project($path, $this->project);
        $this->session_service = $this->parser->service($path, $this->service);
        $this->session_action = $this->parser->action($path, $this->action);

        return $this;
    }

    /**
     * Получить ключ
     * @param string $id ID статуса
     * @return string
     * @throws Exception
     */
    public function key(string $id): string
    {
        $key = "{$this->session_project}-{$this->session_service}-{$this->session_action}__{$id}";
        $this->resetSession();
        if (strlen($this->wrapKey($key)) >= 250) {
            throw new Exception("Total key length exceeded 250 characters");
        }

        return $key;
    }

    /**
     * Сохранить значение статуса
     * @param string $id ID статуса
     * @param mixed $value Значение статуса
     * @param int|null $expires Время хранения статуса
     * @return void
     * @throws Exception
     */
    public function save(string $id, mixed $value, ?int $expires = null): void
    {
        $key = $this->key($id);

        if (!$expires and $this->statusTtl) {
            $expires = $this->statusTtl;
        }

        $this->storage->save($this->wrapKey($key), $value, $expires);
    }

    /**
     * Получить значение статуса
     * @param string $id ID статуса
     * @param mixed|null $default Значение по умалчанию
     * @return mixed
     * @throws Exception
     */
    public function load(string $id, mixed $default = null): mixed
    {
        $key = $this->key($id);

        $value = $this->storage->load($this->wrapKey($key), $default);

        return $value;
    }

    /**
     * Сбросить переменные сессии
     * @return $this
     */
    public function resetSession()
    {
        $this->session_project = $this->project;
        $this->session_service = $this->service;
        $this->session_action = $this->action;

        return $this;
    }

}
