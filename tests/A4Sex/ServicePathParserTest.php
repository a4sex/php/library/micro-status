<?php

namespace A4Sex;

class ServicePathParserTest extends \PHPUnit\Framework\TestCase
{
    private ServicePathParser $object;

    protected function setUp(): void
    {
        $this->object = new ServicePathParser();
    }

    public function testAction()
    {
        self::assertEquals('action', $this->object->action('project/service/action'));
        self::assertEquals('action', $this->object->action('project/service/action', 'default'));
        self::assertEquals('action', $this->object->action('service/action', 'default'));
        self::assertEquals('action', $this->object->action('action'));
    }

    public function testProject()
    {
        self::assertEquals('project', $this->object->project('project/service/action'));
        self::assertEquals('project', $this->object->project('project/service/action', 'default'));
        self::assertEquals('default', $this->object->project('service/action', 'default'));
        self::assertNull($this->object->project('service/action'));
        self::assertNull($this->object->project('action'));
    }

    public function testService()
    {
        self::assertEquals('service', $this->object->service('project/service/action'));
        self::assertEquals('service', $this->object->service('project/service/action', 'default'));
        self::assertEquals('service', $this->object->service('service/action', 'default'));
        self::assertEquals('default', $this->object->service('action', 'default'));
        self::assertNull($this->object->service('action'));
    }

    public function testParse()
    {
        self::assertCount(3, $this->object->parse('project/service/action'));
        self::assertCount(2, $this->object->parse('service/action'));
        self::assertCount(1, $this->object->parse('action'));
    }

}
