<?php

namespace A4Sex\Tests;

use A4Sex\KeyValueStorage;
use A4Sex\MicroStatus;
use A4Sex\ServicePathParser;
use Symfony\Component\Cache\Adapter\ArrayAdapter;
use function PHPUnit\Framework\assertEquals;
use function PHPUnit\Framework\assertNull;

class MicroStatusTest extends \PHPUnit\Framework\TestCase
{
    private MicroStatus $object;

    protected function setUp(): void
    {
        $memory = new ArrayAdapter(5);
        $storage = new KeyValueStorage($memory);
        $parser = new ServicePathParser();
        $this->object = new MicroStatus($parser, $storage);
    }

    public function testKey()
    {
        self::assertEquals('default-service-other__statusId', $this->object->setAction('other')->key('statusId'));
        self::assertEquals('default-service-other__statusId', $this->object->key('statusId'));

        self::assertEquals('default-service-action__statusId', $this->object->setAction()->key('statusId'));
        self::assertEquals('default-service-action__statusId', $this->object->key('statusId'));
    }

    public function testSave()
    {
        self::expectNotToPerformAssertions();
        $this->object->save('k9Ku8CVGLr9ItKtXq7J8QRv1axiH8FGY', 12345);
    }

    public function testService()
    {
        $this->object->setService('anotherService');
        self::assertEquals('default-anotherService-action__statusId', $this->object->key('statusId'));
    }

    public function testProject()
    {
        $this->object->setProject('anotherProject');
        self::assertEquals('anotherProject-service-action__statusId', $this->object->key('statusId'));
    }

    public function testAction()
    {
        $this->object->setAction('some');
        self::assertEquals('default-service-some__statusId', $this->object->key('statusId'));
    }

    public function testLoad()
    {
        $this->object->save('k9Ku8CVGLr9ItKtXq7J8QRv1axiH8FGY', 12345);
        assertEquals(12345, $this->object->load('k9Ku8CVGLr9ItKtXq7J8QRv1axiH8FGY'));
    }

    public function testLoadDefault()
    {
        assertNull($this->object->load('k9Ku8CVGLr9ItKtXq7J8QRv1axiH8FGY'));
        assertEquals('54321', $this->object->load('k9Ku8CVGLr9ItKtXq7J8QRv1axiH8FGY', '54321'));
    }

    public function testChain()
    {
        $this->object->setAction('another')->save('5AVUlHPmheXLQxxMuUkFWNgq8uoANfCX', 12345);
        assertEquals(12345, $this->object->load('5AVUlHPmheXLQxxMuUkFWNgq8uoANfCX'));
        assertNull($this->object->action('other')->load('5AVUlHPmheXLQxxMuUkFWNgq8uoANfCX'));
    }

    public function testSession()
    {
        $this->object->action('another')->save('5AVUlHPmheXLQxxMuUkFWNgq8uoANfCX', 12345);
        assertNull($this->object->load('5AVUlHPmheXLQxxMuUkFWNgq8uoANfCX'));
        assertNull($this->object->action('other')->load('5AVUlHPmheXLQxxMuUkFWNgq8uoANfCX'));
        assertNull($this->object->load('5AVUlHPmheXLQxxMuUkFWNgq8uoANfCX'));
        assertEquals(12345, $this->object->action('another')->load('5AVUlHPmheXLQxxMuUkFWNgq8uoANfCX'));
    }

    public function testParser()
    {
        $this->object->action('another')->save('5AVUlHPmheXLQxxMuUkFWNgq8uoANfCX', 12345);
        assertNull($this->object->load('5AVUlHPmheXLQxxMuUkFWNgq8uoANfCX'));
        assertNull($this->object->action('other')->load('5AVUlHPmheXLQxxMuUkFWNgq8uoANfCX'));
        assertNull($this->object->load('5AVUlHPmheXLQxxMuUkFWNgq8uoANfCX'));
        assertEquals(12345, $this->object->action('another')->load('5AVUlHPmheXLQxxMuUkFWNgq8uoANfCX'));
        assertEquals(12345, $this->object->action('service/another')->load('5AVUlHPmheXLQxxMuUkFWNgq8uoANfCX'));
        assertEquals(12345, $this->object->action('default/service/another')->load('5AVUlHPmheXLQxxMuUkFWNgq8uoANfCX'));

        assertNull($this->object->action('service/other')->load('5AVUlHPmheXLQxxMuUkFWNgq8uoANfCX'));
        assertNull($this->object->action('default/service/other')->load('5AVUlHPmheXLQxxMuUkFWNgq8uoANfCX'));
    }

}
